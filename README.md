# Руководство пользователя для API/JAVADOC для драйвера

storage_service_driver – драйвер, позволяющий работать с storage_service из java-кода. Для инициализации драйвера пользователю необходимо указать адрес и порт сервиса. Например:
```JavaScript
...
StorageServiceDriver storageServiceDriver = new StorageServiceDriver("localhost", "8080");
...
```
При вызове методов драйвера происходит непосредственное обращение к сервису.

## Доступные методы

1. **Чтение записи** – getStorageEntity(Long id)
	* **Входные данные:** ключ для хранилища в виде Long
	* **Функцилнал:** возвращает данные, хранящиеся по переданному ключу
	* **Пример использования:**
```Java
        StorageEntity storageEntity = null;
        try {
            storageEntity = storageServiceDriver.getStorageEntity(1L);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            System.out.println(objectMapper.writeValueAsString(storageEntity));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
```
2. **Запись** – setStorageEntity(StorageEntity storageEntity)
	* **Входные данные:** ключ хранилища (Long), данные хранилища (String) и опциональный параметр продолжительности жизни записи в секундах (Long) в виде сущности StorageEntity
	* **Функционал:** если по переданному ключу уже хранятся данные, они будут заменены, иначе будет добавлена новая запись. Метод возвращает метку успешности или неуспешности операции.
	* **Пример использования:**
 ```Java
        StorageEntity yetAnotherSA = new StorageEntity();
        yetAnotherSA.setId(9L);
        yetAnotherSA.setValue("Text");
        yetAnotherSA.setTtl(250L);

        try {
            System.out.println(objectMapper.writeValueAsString(storageServiceDriver.setStorageEntity(yetAnotherSA)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
```
3. **Удаление записи** – removeStorageEntity(Long id)
	* **Входные данные:** ключ для хранилища в виде Long
	* **Функционал:** удаляет запись по переданному ключу. Возвращает данные, которые хранились по переданному ключу или null, при их отсутствии
	* **Пример использования:** 
 ```Java
        try {
            System.out.println(storageServiceDriver.removeStorageEntity(1L));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
```
4. **Сохранение текущего состояния** – dumpStorageEntity()
	* **Входные данные:** –
	* **Функционал:** сохраняет текущее состояние хранилища и возвращает его в виде загружаемого файла
	* **Пример использования:**
 ```Java
File original = storageServiceDriver.dumpStorageEntity();
```
5. **Загрузка состояния хранилища** – loadDumpStorageEntity(File dump)
	* **Входные данные:** файл с сохранённым состоянием хранилища
	* **Функционал:** загружает состояние хранилища из файла
	* **Пример использования:** 
 ```Java
        try {
            System.out.println(storageServiceDriver.loadDumpStorageEntity(copied));
        } catch (IOException e) {
            e.printStackTrace();
        }
```
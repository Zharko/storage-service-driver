package storage.service.driver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;
import storage.service.entity.StorageEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class StorageServiceDriver {
    private final String address;
    private final String port;

    public StorageServiceDriver(String address, String port) {
        this.address = address;
        this.port = port;
    }

    private static final String REQ_VALUE = "/req/value/";
    private static final String REQ_DUMP = "/req/dump/";

    private String getUrl() {
        return "http://" + address + ":" + port;
    }

    private RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    public StorageEntity getStorageEntity(Long id) throws JsonProcessingException {
        ResponseEntity<String> responseEntity = getRestTemplate()
                .getForEntity(getUrl() + REQ_VALUE + id, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(responseEntity.getBody(), StorageEntity.class);
    }

    public String setStorageEntity(StorageEntity storageEntity) throws JsonProcessingException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(storageEntity);

        HttpEntity<String> httpEntity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<String> responseEntity = getRestTemplate()
                .postForEntity(getUrl() + REQ_VALUE, httpEntity, String.class);

        return responseEntity.getBody();
    }

    public StorageEntity removeStorageEntity(Long id) throws JsonProcessingException {
        ResponseEntity<String> responseEntity = getRestTemplate()
                .exchange(getUrl() + REQ_VALUE + id, HttpMethod.DELETE, null, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(responseEntity.getBody(), StorageEntity.class);
    }

    public File dumpStorageEntity() {
        File file = getRestTemplate().execute(getUrl() + REQ_DUMP, HttpMethod.GET,
                clientHttpRequest -> clientHttpRequest
                        .getHeaders()
                        .setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL)),
                clientHttpResponse -> {
                    File ret = File.createTempFile("download", "tmp");
                    StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(ret));
                    return ret;
                });

        return file;
    }

    public String loadDumpStorageEntity(File dump) throws IOException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap multiValueMap = new LinkedMultiValueMap<>();
        multiValueMap.add("file", new FileSystemResource(dump));

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(multiValueMap, httpHeaders);

        ResponseEntity<String> responseEntity = getRestTemplate()
                .postForEntity(getUrl() + REQ_DUMP, requestEntity, String.class);

        return responseEntity.getBody();
    }

}

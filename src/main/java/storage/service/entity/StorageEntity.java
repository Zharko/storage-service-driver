package storage.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StorageEntity {
    private Long id;
    private String value;
    @JsonIgnore
    private String createdAt;
    @JsonIgnore
    private String deletedAt;
    private Long ttl;
}
